<?php

namespace backend\modules;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class BackendController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'create', 'update', 'delete', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * comment to remove override for json format
	 *
	 * @param $action
	 * @return bool
	 * @throws \yii\web\BadRequestHttpException
	 */
	public function beforeAction($action)
	{

		Yii::$app->response->format = Response::FORMAT_JSON;
		return parent::beforeAction($action);
	}

	/**
	 * comment to remove override for json format
	 *
	 * @param string $view
	 * @param array $params
	 * @return array|string
	 */
	public function render($view, $params = [])
	{
		return $params;
	}

}