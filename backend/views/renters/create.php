<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Renters */

$this->title = Yii::t('backend', 'Create Renters');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Renters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="renters-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
