<?php

namespace common\models;

use Yii;
use common\models\query\ContractsQuery;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "contracts".
 *
 * @property int $id
 * @property int $id_user
 * @property string $number
 * @property string $type
 * @property string $date_start
 * @property string $date_end
 * @property string $rent
 * @property string $price
 *
 * @property Users $user
 * @property Properties[] $properties
 */
class Contracts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contracts';
    }

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user'], 'integer'],
            [['number'], 'required'],
            [['type'], 'string'],
            [['date_start', 'date_end'], 'safe'],
            [['rent', 'price'], 'number'],
            [['number'], 'string', 'max' => 255],
            [['number'], 'unique'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'id_user' => Yii::t('model', 'Id User'),
            'number' => Yii::t('model', 'Number'),
            'type' => Yii::t('model', 'Type'),
            'date_start' => Yii::t('model', 'Date Start'),
            'date_end' => Yii::t('model', 'Date End'),
            'rent' => Yii::t('model', 'Rent'),
            'price' => Yii::t('model', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(Properties::className(), ['id_contract' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ContractsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContractsQuery(get_called_class());
    }

    public function beforeValidate() {
    	$this->id_user = Yii::$app->user->id;

    	return parent::beforeValidate();
    }

}
