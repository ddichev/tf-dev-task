<?php

namespace common\models;

use Yii;
use common\models\query\PropertiesQuery;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "properties".
 *
 * @property int $id
 * @property int $id_contract
 * @property int $id_user
 * @property string $number
 * @property string $rent
 *
 * @property Contracts $contract
 * @property Users $user
 * @property PropertiesRenters[] $propertiesRenters
 * @property Renters[] $renters
 */
class Properties extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'properties';
    }

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_contract', 'id_user'], 'integer'],
            [['number', 'rent'], 'required'],
            [['rent'], 'number'],
            [['number'], 'string', 'max' => 255],
            [['number'], 'unique'],
            [['id_contract'], 'exist', 'skipOnError' => true, 'targetClass' => Contracts::className(), 'targetAttribute' => ['id_contract' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'id_contract' => Yii::t('model', 'Id Contract'),
            'id_user' => Yii::t('model', 'Id User'),
            'number' => Yii::t('model', 'Number'),
            'rent' => Yii::t('model', 'Rent'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contracts::className(), ['id' => 'id_contract']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertiesRenters()
    {
        return $this->hasMany(PropertiesRenters::className(), ['id_property' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRenters()
    {
        return $this->hasMany(Renters::className(), ['id' => 'id_renter'])->viaTable('properties_renters', ['id_property' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return PropertiesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PropertiesQuery(get_called_class());
    }

	public function beforeValidate() {
		$this->id_user = Yii::$app->user->id;

		return parent::beforeValidate();
	}
}
