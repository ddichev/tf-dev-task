<?php

namespace common\models;

use Yii;
use common\models\query\PropertiesRentersQuery;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "properties_renters".
 *
 * @property int $id_property
 * @property int $id_renter
 * @property string $percent_property
 *
 * @property Properties $property
 * @property Renters $renter
 */
class PropertiesRenters extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'properties_renters';
    }

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_property', 'id_renter'], 'required'],
            [['id_property', 'id_renter'], 'integer'],
            [['percent_property'], 'number'],
            [['id_property', 'id_renter'], 'unique', 'targetAttribute' => ['id_property', 'id_renter']],
            [['id_property'], 'exist', 'skipOnError' => true, 'targetClass' => Properties::className(), 'targetAttribute' => ['id_property' => 'id']],
            [['id_renter'], 'exist', 'skipOnError' => true, 'targetClass' => Renters::className(), 'targetAttribute' => ['id_renter' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_property' => Yii::t('model', 'Id Property'),
            'id_renter' => Yii::t('model', 'Id Renter'),
            'percent_property' => Yii::t('model', 'Percent Property'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'id_property']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRenter()
    {
        return $this->hasOne(Renters::className(), ['id' => 'id_renter']);
    }

    /**
     * {@inheritdoc}
     * @return PropertiesRentersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PropertiesRentersQuery(get_called_class());
    }
}
