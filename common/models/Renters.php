<?php

namespace common\models;

use Yii;
use common\models\query\RentersQuery;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "renters".
 *
 * @property int $id
 * @property string $full_name
 * @property string $phone
 * @property string $egn
 *
 * @property PropertiesRenters[] $propertiesRenters
 * @property Properties[] $properties
 */
class Renters extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renters';
    }

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'phone', 'egn'], 'required'],
            [['full_name', 'phone', 'egn'], 'string', 'max' => 255],
            [['full_name'], 'unique'],
            [['phone'], 'unique'],
            [['egn'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'full_name' => Yii::t('model', 'Full Name'),
            'phone' => Yii::t('model', 'Phone'),
            'egn' => Yii::t('model', 'Egn'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertiesRenters()
    {
        return $this->hasMany(PropertiesRenters::className(), ['id_renter' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(Properties::className(), ['id' => 'id_property'])->viaTable('properties_renters', ['id_renter' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return RentersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RentersQuery(get_called_class());
    }
}
