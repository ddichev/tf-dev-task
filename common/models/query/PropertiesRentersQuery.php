<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[PropertiesRenters]].
 *
 * @see PropertiesRenters
 */
class PropertiesRentersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return common\models\PropertiesRenters[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PropertiesRenters|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
