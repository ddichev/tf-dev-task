<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[Renters]].
 *
 * @see Renters
 */
class RentersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Renters[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Renters|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
