<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

	    $this->createTable('{{%contracts}}', [
		    'id' => $this->primaryKey(),
		    'id_user' => $this->integer(11)->notNull(),
		    'number' => $this->string()->notNull()->unique(),
		    'type' => "ENUM('rent', 'property')",
		    'date_start' => $this->timestamp()->null()->defaultExpression('NULL'),
		    'date_end' => $this->timestamp()->null()->defaultExpression('NULL'),
		    'rent' => $this->decimal(),
		    'price' => $this->decimal(),

		    'created_at' => $this->integer()->notNull(),
		    'updated_at' => $this->integer()->notNull(),
	    ], $tableOptions);

	    $this->createIndex(
		    'idx-contract-id_users',
		    'contracts',
		    'id_user'
	    );

	    $this->addForeignKey(
		    'fk-contracts-id_user',
		    'contracts',
		    'id_user',
		    'users',
		    'id',
		    'CASCADE',
		    'CASCADE'
	    );

	    $this->createTable('{{%properties}}', [
		    'id' => $this->primaryKey(),
		    'id_contract' => $this->integer(11)->null(),
		    'id_user' => $this->integer(11)->notNull(),
		    'number' => $this->string()->notNull()->unique(),
		    'rent' => $this->decimal()->notNull(),


		    'created_at' => $this->integer()->notNull(),
		    'updated_at' => $this->integer()->notNull(),
	    ], $tableOptions);

	    $this->createIndex(
		    'idx-properties-id_contract',
		    'properties',
		    'id_contract'
	    );

	    $this->addForeignKey(
		    'fk-properties-id_contract',
		    'properties',
		    'id_contract',
		    'contracts',
		    'id',
		    'SET NULL'
	    );

	    $this->createIndex(
		    'idx-properties-id_users',
		    'properties',
		    'id_user'
	    );

	    $this->addForeignKey(
		    'fk-properties-id_user',
		    'properties',
		    'id_user',
		    'users',
		    'id',
		    'CASCADE',
		    'CASCADE'
	    );

	    $this->createTable('{{%renters}}', [
		    'id' => $this->primaryKey(),
		    'full_name' => $this->string()->notNull()->unique(),
		    'phone' => $this->string()->notNull()->unique(),
		    'egn' => $this->string()->notNull()->unique(),

		    'created_at' => $this->integer()->notNull(),
		    'updated_at' => $this->integer()->notNull(),
	    ], $tableOptions);

	    $this->createTable('{{%properties_renters}}', [
		    'id_property' => $this->integer(),
		    'id_renter' => $this->integer(),
		    'percent_property' => $this->decimal(),

		    'created_at' => $this->integer()->notNull(),
		    'updated_at' => $this->integer()->notNull(),

		    'PRIMARY KEY(id_property, id_renter)',
	    ], $tableOptions);

	    $this->createIndex(
		    'idx-properties_renters-id_property',
		    'properties_renters',
		    'id_property'
	    );

	    $this->addForeignKey(
		    'fk-properties_renters-id_property',
		    'properties_renters',
		    'id_property',
		    'properties',
		    'id',
		    'CASCADE',
		    'CASCADE'
	    );

	    $this->createIndex(
		    'idx-properties_renters-id_renter',
		    'properties_renters',
		    'id_renter'
	    );

	    $this->addForeignKey(
		    'fk-properties_renters-id_renter',
		    'properties_renters',
		    'id_renter',
		    'renters',
		    'id',
		    'CASCADE',
		    'CASCADE'
	    );
    }

    public function down()
    {
    	$this->dropForeignKey('fk-properties_renters-id_renter', 'properties_renters');
    	$this->dropForeignKey('fk-properties_renters-id_property', 'properties_renters');
    	$this->dropForeignKey('fk-properties-id_user', 'properties');
    	$this->dropForeignKey('fk-properties-id_contract','properties');
    	$this->dropForeignKey('fk-contracts-id_user', 'contracts');
	    $this->dropTable('{{%properties_renters}}');
	    $this->dropTable('{{%properties}}');
	    $this->dropTable('{{%renters}}');
	    $this->dropTable('{{%contracts}}');
	    $this->dropTable('{{%users}}');
    }
}
